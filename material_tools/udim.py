# SPDX-FileCopyrightText: 2016-2025, Thomas Larsson
#
# SPDX-License-Identifier: GPL-2.0-or-later

import bpy
import os
from ..error import *
from ..utils import *
from ..fileutils import MultiFile, ImageFile
from ..material import LocalTextureSaver
from ..matsel import MaterialSelector
from ..tree import getFromSocket, XSIZE, YSIZE, YSTEP
from ..merge_uvs import TileFixer, getTileBase

#----------------------------------------------------------
#   Tiles From Graft
#----------------------------------------------------------

class DAZ_OT_TilesFromGraft(DazPropsOperator, TileFixer, IsMesh):
    bl_idname = "daz.tiles_from_geograft"
    bl_label = "Tiles From Geograft"
    bl_description = "Move geograft UV coordinates to same tile as body UVs"
    bl_options = {'UNDO'}

    def run(self, context):
        hum = context.object
        for graft in getSelectedMeshes(context):
            if graft != hum:
                self.udimsFromGraft(graft, hum)

#----------------------------------------------------------
#   Fix Texture Tiles
#----------------------------------------------------------

class DAZ_OT_FixTextureTiles(DazOperator, LocalTextureSaver, TileFixer):
    bl_idname = "daz.fix_texture_tiles"
    bl_label = "Fix Texture Tiles"
    bl_description = "Copy textures to the right directory and correct tile numbers.\nTo fix incorrect Genesis 8.1 material names"
    bl_options = {'UNDO'}

    @classmethod
    def poll(self, context):
        ob = context.object
        return (ob and dazRna(ob).DazLocalTextures)

    def run(self, context):
        ob = context.object
        self.findMatTiles(ob)
        self.fixTextures(ob, ob.active_material.name)

#----------------------------------------------------------
#   Make UDIM materials
#----------------------------------------------------------

def getTargetMaterial(scn, context):
    ob = context.object
    return [(mat.name, mat.name, mat.name) for mat in ob.data.materials]


class DAZ_OT_MakeUdimMaterials(DazPropsOperator, LocalTextureSaver, MaterialSelector, TileFixer):
    bl_idname = "daz.make_udim_materials"
    bl_label = "Make UDIM Materials"
    bl_description = "Combine materials of selected mesh into a single UDIM material.\nGeografts must be merged first"
    bl_options = {'UNDO'}

    trgmat : EnumProperty(items=getTargetMaterial, name="Active")

    useGuessMissing : BoolProperty(
        name = "Guess Missing Textures",
        description = "Search for UDIM textures that almost match location in node tree",
        default = True)

    useSaveLocalTextures : BoolProperty(
        name = "Save Local Textures",
        description = "Save local textures if not already done",
        default = True)

    keepDirs = True

    useFixTextures : BoolProperty(
        name = "Fix Textures",
        description = (
            "Copy textures to the right directory and correct tile numbers.\n" +
            "For incorrect Genesis 8.1 material names,\n" +
            "or for textures without tile info"),
        default = True)

    useMergeMaterials : BoolProperty(
        name = "Merge Materials",
        description = "Merge materials and not only textures.\nThis may cause some information loss.\nIf not, Merge Materials can be called afterwards",
        default = False)

    useStackShells : BoolProperty(
        name = "Stack Shells",
        description = "Add shell groups to UDIM material",
        default = True)

    def draw(self, context):
        self.layout.prop(self, "useSaveLocalTextures")
        self.layout.prop(self, "useFixTextures")
        self.layout.prop(self, "useMergeMaterials")
        if self.useMergeMaterials:
            self.layout.prop(self, "useStackShells")
        self.layout.prop(self, "trgmat")
        self.layout.prop(self, "useGuessMissing")
        self.layout.label(text="Materials To Merge")
        MaterialSelector.draw(self, context)


    def invoke(self, context, event):
        self.setupMaterialSelector(context)
        return DazPropsOperator.invoke(self, context, event)


    def isDefaultActive(self, mat, ob):
        return self.isSkinRedMaterial(mat)


    def run(self, context):
        ob = context.object
        if not dazRna(ob).DazLocalTextures:
            if self.useSaveLocalTextures:
                self.saveLocalTextures(context)
            else:
                raise DazError("Save local textures first")
        if self.useFixTextures:
            self.findMatTiles(ob)
            self.fixTextures(ob, self.trgmat)

        mats = []
        mnums = []
        actmat = None
        for mn,umat in enumerate(self.umats):
            if umat.bool:
                mat = ob.data.materials[umat.name]
                mats.append(mat)
                mnums.append(mn)
                if actmat is None or mat.name == self.trgmat:
                    actmat = mat
                    amnum = mn
                    acttile = 1001 + dazRna(mat).DazUDim

        if actmat is None:
            raise DazError("No materials selected")

        texnodes = {}
        hasmapping = False
        for mat in mats:
            nodes,hasmaps = self.getTextureNodes(mat)
            texnodes[mat.name] = nodes
            if mat == actmat:
                hasmapping = hasmaps

        if self.useMergeMaterials and self.useStackShells:
            shells0 = self.getShells(mats)
            actshells = self.getShells([actmat])
            shells = {}
            for tname,data in shells0.items():
                if tname not in actshells.keys():
                    shells[tname] = data

        basenames = {}
        for key,actnode in texnodes[actmat.name].items():
            actnode.image.source = "TILED"
            actnode.extension = "CLIP"
            if actnode.image:
                imgname = actnode.image.name
            else:
                imgname = actnode.name
            basename = "T_%s" % self.getBaseName(imgname, actdazRna(mat).DazUDim)
            udims = {}
            for mat in mats:
                nodes = texnodes[mat.name]
                node = nodes.get(key)
                found = True
                if node is None and self.useGuessMissing:
                    if key.endswith((":A", ":B")):
                        node = nodes.get(key[:-2])
                        found = False
                        if node.image:
                            basenames[node.image.filepath] = basename
                if node and node.image:
                    img = node.image
                    if found:
                        self.updateImage(img, basename, dazRna(mat).DazUDim)
                    if dazRna(mat).DazUDim not in udims.keys():
                        udims[dazRna(mat).DazUDim] = mat.name
                    if mat == actmat:
                        img.name = self.makeImageName(basename, acttile, img)
                        node.label = basename
                        node.name = basename

            img = actnode.image
            if bpy.app.version >= (3, 1, 0):
                path2,ext2 = os.path.splitext(img.filepath)
                tile,base = getTileBase(path2)
                if base:
                    img.filepath = "%s_<UDIM>%s" % (base,ext2)
            tile0 = img.tiles[0]
            for udim,mname in udims.items():
                if udim == 0:
                    tile0.number = 1001
                    tile0.label = mname
                else:
                    img.tiles.new(tile_number=1001+udim, label=mname)

        for mat in mats:
            self.addSkipZeroUvs(mat)

        if self.useMergeMaterials:
            for f in ob.data.polygons:
                if f.material_index in mnums:
                    f.material_index = amnum

            mnums.reverse()
            for mn in mnums:
                if mn != amnum:
                    ob.data.materials.pop(index=mn)
            if self.useStackShells:
                self.addShells(actmat, shells)
        else:
            actnodes = texnodes[actmat.name]
            for mat in mats:
                if mat != actmat:
                    nodes = texnodes[mat.name]
                    for key,node in nodes.items():
                        actnode = actnodes.get(key)
                        if actnode is None and node.image and self.useGuessMissing:
                            actnode = self.findBestMatch(key, node.image, mat, actnodes, basenames)
                        if actnode:
                            img = node.image = actnode.image
                            node.extension = "CLIP"
                            node.label = actnode.label
                            node.name = actnode.name


    def makeImageName(self, basename, tile, img):
        return "%s%s" % (basename, os.path.splitext(img.name)[1])


    def getTextureNodes(self, mat):
        def getChannel(node, links):
            for link in links:
                if link.from_node == node:
                    sname = link.to_socket.name
                    if link.to_node.type in ['MIX_RGB', 'MIX', 'MATH', 'GAMMA']:
                        return "%s:%s" % (getChannel(link.to_node, links), sname)
                    elif link.to_node.type == 'BSDF_PRINCIPLED':
                        return "PBR:%s" % sname
                    elif link.to_node.type == 'GROUP':
                        return "%s:%s" % (link.to_node.node_tree.name, sname)
                    else:
                        return "%s:%s" % (link.to_node.type, sname)
            return None

        texnodes = {}
        hasmaps = False
        for node in mat.node_tree.nodes:
            if node.type == 'TEX_IMAGE' and node.image:
                if node.image.source == "TILED":
                    raise DazError("Material %s is already an UDIM material" % mat.name)
                channel = getChannel(node, mat.node_tree.links)
                links = node.inputs["Vector"].links
                if links and links[0].from_node.type == 'MAPPING':
                    hasmaps = True
                elif channel in texnodes.keys():
                    print("Duplicate channel: %s" % channel)
                else:
                    texnodes[channel] = node
        return texnodes, hasmaps


    def getBaseName(self, path, udim):
        tile,base = getTileBase(os.path.splitext(path)[0])
        if tile == 1001+udim:
            return base
        return path


    def updateImage(self, img, basename, udim):
        src = bpy.path.abspath(img.filepath)
        src = bpy.path.reduce_dirs([src])[0]
        folder = os.path.dirname(src)
        fname,ext = os.path.splitext(bpy.path.basename(src))
        if fname[-6:] == '<UDIM>':
            src = os.path.join(folder, "%s%d%s" % (fname[:-6], 1001+udim, ext))
        trg = os.path.join(folder, "%s_%d%s" % (basename, 1001+udim, ext))
        self.changeImage(src, trg, img)


    def findBestMatch(self, key, img, mat, actnodes, basenames):
        for ext in ["A", "B"]:
            actnode = actnodes.get("%s:%s" % (key, ext))
            if actnode and actnode.image:
                folder1 = os.path.dirname(img.filepath)
                folder2 = os.path.dirname(actnode.image.filepath)
                if folder1 == folder2:
                    basename = basenames.get(img.filepath)
                    if basename:
                        self.updateImage(img, basename, dazRna(mat).DazUDim)
                    return actnode
        return None


    def getShells(self, mats):
        from ..tree import getFromNode
        from ..matsel import isShellNode
        nodes = {}
        for mat in mats:
            if mat.node_tree:
                n = len(mat.name)
                for node in mat.node_tree.nodes:
                    if isShellNode(node):
                        tree = node.node_tree
                        if tree.name[-n:] == mat.name:
                            shname = tree.name[:-n-1]
                            uvmap = getFromNode(node.inputs["UV"])
                            if uvmap:
                                nodes[shname] = (node, uvmap.uv_map)
        return nodes


    def addShells(self, mat, shells):
        if not shells:
            return
        from ..cycles import makeCyclesTree
        from ..cgroup import SkipZeroUvGroup
        from ..tree import findNodes
        ctree = makeCyclesTree(mat)
        for outp in findNodes(mat.node_tree, 'OUTPUT_MATERIAL'):
            x,y = outp.location
            outp.location = (x+2*XSIZE, y)
            ssocket = getFromSocket(outp.inputs["Surface"])
            dsocket = getFromSocket(outp.inputs["Displacement"])
            for tname,data in shells.items():
                template,uvname = data
                uvmap = ctree.addNode("ShaderNodeUVMap")
                uvmap.uv_map = uvname
                uvmap.label = uvname
                uvmap.hide = True
                uvmap.location = (x,y-6*YSTEP)
                skip = ctree.addGroup(SkipZeroUvGroup, "DAZ Skip Zero UVs")
                skip.location = (x,y)
                ctree.links.new(uvmap.outputs["UV"], skip.inputs["UV"])
                shell = ctree.addNode("ShaderNodeGroup")
                shell.location = (x+XSIZE, y)
                shell.node_tree = template.node_tree
                shell.label = template.label
                ctree.links.new(skip.outputs["Influence"], shell.inputs["Influence"])
                ctree.links.new(uvmap.outputs["UV"], shell.inputs["UV"])
                if ssocket:
                    ctree.links.new(ssocket, shell.inputs["BSDF"])
                if dsocket:
                    ctree.links.new(dsocket, shell.inputs["Displacement"])
                ssocket = shell.outputs["BSDF"]
                dsocket = shell.outputs["Displacement"]
                y -= YSIZE
            if ssocket:
               ctree.links.new(ssocket, outp.inputs["Surface"])
            if dsocket:
                ctree.links.new(dsocket, outp.inputs["Displacement"])

#----------------------------------------------------------
#   Shift UVs
#----------------------------------------------------------

def getUVDims(tile):
    tile = tile - 1001
    vdim = tile//10
    udim = tile - 10*vdim
    return udim,vdim


def shiftUVs(mat, mn, ob, udim, vdim):
    ushift = udim - dazRna(mat).DazUDim
    vshift = vdim - dazRna(mat).DazVDim
    print(" Shift", mat.name, mn, ushift, vshift)
    if ushift == 0 and vshift == 0:
        return
    uvlayer = ob.data.uv_layers.active
    m = 0
    for fn,f in enumerate(ob.data.polygons):
        if f.material_index == mn:
            for n in range(len(f.vertices)):
                uv = uvlayer.data[m].uv
                uv[0] += ushift
                uv[1] += vshift
                m += 1
        else:
            m += len(f.vertices)

#----------------------------------------------------------
#   Set Udims to given tile
#----------------------------------------------------------

class DAZ_OT_SetUDims(DazPropsOperator, MaterialSelector):
    bl_idname = "daz.set_udims"
    bl_label = "Set UDIM Tile"
    bl_description = "Move all UV coordinates of selected materials to specified UV tile"
    bl_options = {'UNDO'}

    tile : IntProperty(name="Tile", min=1001, max=1100, default=1001)

    def draw(self, context):
        self.layout.prop(self, "tile")
        MaterialSelector.draw(self, context)


    def invoke(self, context, event):
        self.setupMaterialSelector(context)
        return DazPropsOperator.invoke(self, context, event)


    def isDefaultActive(self, mat, ob):
        return False


    def run(self, context):
        from ..material import addUdimTree
        ob = context.object
        udim,vdim = getUVDims(self.tile)
        for mn,umat in enumerate(self.umats):
            if umat.bool:
                mat = ob.data.materials[umat.name]
                shiftUVs(mat, mn, ob, udim, vdim)
                addUdimTree(mat.node_tree, udim, vdim)
                dazRna(mat).DazUDim = udim
                dazRna(mat).DazVDim = vdim

#----------------------------------------------------------
#   Initialize
#----------------------------------------------------------

classes = [
    DAZ_OT_TilesFromGraft,
    DAZ_OT_FixTextureTiles,
    DAZ_OT_MakeUdimMaterials,
    DAZ_OT_SetUDims,
]

def register():
    for cls in classes:
        bpy.utils.register_class(cls)

def unregister():
    for cls in classes:
        bpy.utils.unregister_class(cls)



