# SPDX-FileCopyrightText: 2016-2025, Thomas Larsson
#
# SPDX-License-Identifier: GPL-2.0-or-later

import bpy
from mathutils import *
from .utils import *
from .transform import Transform
from .error import *
from .node import Node, Instance
from .bone_data import BD

#-------------------------------------------------------------
#   Alternative bone names
#-------------------------------------------------------------

def getMappedBone(bname, rig, mesh=None):
    def getBone(bname):
        bname = unquote(bname)
        pg = dazRna(rig.data).DazBoneMap.get(bname)
        if pg and pg.s in rig.data.bones.keys():
            return pg.s
        elif bname in rig.pose.bones.keys():
            return bname
        bname1 = BD.BoneMap.get(bname, "")
        if bname1 and bname1 in rig.data.bones.keys():
            return bname1
        from .fix import getSuffixName
        sufname = getSuffixName(bname, False)
        if sufname in rig.pose.bones.keys():
            return sufname
        sufname1 = getSuffixName(bname1, False)
        if sufname1 in rig.pose.bones.keys():
            return sufname1
        return ""

    if rig is None or rig.data is None or bname is None:
        return None
    bname1 = getBone(bname)
    if bname1:
        return bname1
    elif mesh and ":" not in bname:
        bname1 = "%s:%s" % (mesh.name, bname)
        return getBone(bname1)
    return ""

#-------------------------------------------------------------
#   BoneInstance
#-------------------------------------------------------------

class BoneInstance(Instance):

    def __init__(self, fileref, node, struct):
        from .figure import FigureInstance
        Instance.__init__(self, fileref, node, struct)
        if isinstance(self.parent, FigureInstance):
            self.figure = self.parent
        elif isinstance(self.parent, BoneInstance):
            self.figure = self.parent.figure
        self.id = self.node.id.rsplit("#",1)[-1]
        self.name = self.node.name
        self.roll = 0.0
        self.useRoll = False
        #node.inherits_scale = False
        self.axes = [0,1,2]
        self.flipped = [False,False,False]
        self.flopped = [False,False,False]
        self.isPosed = False
        self.isBuilt = False
        self.test = False
        self.ignoreBone = (GS.ignoreG9TwistBones and self.name.endswith(("twist1", "twist2")))


    def __repr__(self):
        pname = (self.parent.id if self.parent else None)
        fname = (self.figure.name if self.figure else None)
        return "<BoneInst %s N: %s F: %s T: %s P: %s R:%s>" % (self.id, self.node.name, fname, self.target, pname, self.rna)


    def parentObject(self, context, ob):
        pass


    def buildExtra(self, context):
        pass


    def finalize(self, context):
        pass


    def getHeadTail(self, center, mayfit=True):
        if mayfit and self.restdata:
            return self.restdata
        else:
            from .dbzfile import DBZRestData
            head = (self.attributes["center_point"] - center)
            tail = (self.attributes["end_point"] - center)
            orient = Euler(self.attributes["orientation"]*D)
            xyz = self.rotation_order
            wsmat = self.U3
            dazhead = head
            return DBZRestData(head, tail, orient, xyz, None, wsmat, dazhead)


    RX = Matrix.Rotation(pi/2, 4, 'X')
    FX = Matrix.Rotation(pi, 4, 'X')
    FZ = Matrix.Rotation(pi, 4, 'Z')

    def buildEdit(self, figure, figinst, rig, parent, center, isFace):
        if self.ignoreBone:
            return
        self.makeNameUnique(rig.data.edit_bones)
        rdata = self.getHeadTail(center)
        eb = rig.data.edit_bones.new(self.name)
        figure.bones[self.name] = eb.name
        figinst.bones[self.name] = self
        rdata.checkBone(self.name)
        eb.parent = parent
        eb.head = head = d2b(rdata.head)
        eb.tail = tail = d2b(rdata.tail)
        length = (head-tail).length
        omat = rdata.orient.to_matrix()
        lsmat = self.getLocalMatrix(rdata.wsmat, omat)
        if not eulerIsZero(lsmat.to_euler()):
            self.isPosed = True
        omat = omat.to_4x4()
        if GS.zup:
            omat = self.RX @ omat
        flip = self.FX
        if not GS.unflipped:
            omat,flip = self.flipAxes(omat, rdata.xyz)

        if self.test:
            print("BB", self.name, rdata.orient)

        #  engetudouiti's fix for posed bones
        rmat = rdata.wsmat.to_4x4()
        if GS.zup:
            rmat = self.RX @ rmat @ self.RX.inverted()
        if rmat.determinant() > 1e-4:
            omat = rmat.inverted() @ omat

        if GS.unflipped:
            omat.col[3][0:3] = head
            eb.matrix = omat
        else:
            omat = self.flipBone(omat, head, tail, flip)
            self.setFlip()
            if self.test:
                print("FBONE", self.name, self.rotation_order, self.axes, self.flipped)
            omat.col[3][0:3] = head
            eb.matrix = omat
            self.correctRoll(eb, figure)
        self.correctLength(eb, length)

        if self.name in BD.FaceRigs:
            isFace = True
        trgname = BD.Irises.get(eb.name)
        if trgname:
            trg = rig.data.edit_bones.get(trgname)
            if trg:
                eb.tail = tail = trg.tail
        for child in self.children.values():
            if isinstance(child, BoneInstance):
                child.buildEdit(figure, figinst, rig, eb, center, isFace)
        self.isBuilt = True


    def makeNameUnique(self, ebones):
        if self.name not in ebones.keys():
            return
        orig = self.name
        if len(self.name) < 2:
            self.name = "%s-1" % self.name
        while self.name in ebones.keys():
            if self.name[-2] == "-" and self.name[-1].isdigit():
                self.name = "%s-%d" % (self.name[:-2], 1+int(self.name[-1]))
            else:
                self.name = "%s-1" % self.name
        print("Bone name made unique: %s => %s" % (orig, self.name))


    def flipAxes(self, omat, xyz):
        if xyz == 'YZX':    #
            # Blender orientation: Y = twist, X = bend
            euler = Euler((0,0,0))
            flip = self.FX
            self.axes = [0,1,2]
            self.flipped = [False,False,False]
            self.flopped = [False,True,True]
        elif xyz == 'YXZ':
            # Apparently not used
            euler = Euler((0, pi/2, 0))
            flip = self.FZ
            self.axes = [2,1,0]
            self.flipped = [False,False,False]
            self.flopped = [False,False,False]
        elif xyz == 'ZYX':  #
            euler = Euler((pi/2, 0, 0))
            flip = self.FX
            self.axes = [0,2,1]
            self.flipped = [False,True,False]
            self.flopped = [False,False,False]
        elif xyz == 'XZY':  #
            euler = Euler((0, 0, pi/2))
            flip = self.FZ
            self.axes = [1,0,2]
            self.flipped = [False,False,False]
            self.flopped = [False,True,False]
        elif xyz == 'ZXY':
            # Eyes and eyelids
            euler = Euler((pi/2, 0, 0))
            flip = self.FZ
            self.axes = [0,2,1]
            self.flipped = [False,True,False]
            self.flopped = [False,False,False]
        elif xyz == 'XYZ':  #
            euler = Euler((pi/2, pi/2, 0))
            flip = self.FZ
            self.axes = [1,2,0]
            self.flipped = [True,True,True]
            self.flopped = [True,True,False]

        if self.test:
            print("\nAXES", self.name, xyz, self.axes)
        rmat = euler.to_matrix().to_4x4()
        omat = omat @ rmat
        return omat, flip


    def flipBone(self, omat, head, tail, flip):
        vec = tail-head
        yaxis = Vector(omat.col[1][0:3])
        if vec.dot(yaxis) < 0:
            if self.test:
                print("FLOP", self.name)
            self.flipped = self.flopped
            return omat @ flip
        else:
            return omat


    def correctRoll(self, eb, figure):
        if eb.name in BD.RollCorrection.keys():
            offset = BD.RollCorrection[eb.name]
        elif (figure.rigtype in ["genesis1", "genesis2"] and
              eb.name in BD.RollCorrectionG12.keys()):
            offset = BD.RollCorrectionG12[eb.name]
        else:
            return

        roll = eb.roll + offset*D
        if roll > pi:
            roll -= 2*pi
        elif roll < -pi:
            roll += 2*pi
        eb.roll = roll

        a = self.axes
        f = self.flipped
        i = a.index(0)
        j = a.index(1)
        k = a.index(2)
        if offset == 90:
            tmp = a[i]
            a[i] = a[k]
            a[k] = tmp
            tmp = f[i]
            f[i] = not f[k]
            f[k] = tmp
        elif offset == -90:
            tmp = a[i]
            a[i] = a[k]
            a[k] = tmp
            tmp = f[i]
            f[i] = not f[k]
            f[k] = tmp
        elif offset == 180:
            f[i] = not f[i]
            f[k] = not f[k]


    def correctLength(self, eb, length):
        vec = (eb.tail - eb.head).normalized()
        eb.tail = eb.head + length*vec


    def defaultInherit(self):
        return ('FULL' if (self.inherits_scale or GS.useInheritScale) else 'NONE')


    def buildBoneProps(self, rig, center):
        if self.name not in rig.data.bones.keys():
            return
        pb = rig.pose.bones[self.name]
        bone = pb.bone
        bone.inherit_scale = self.defaultInherit()
        dazRna(bone).DazOrient = self.attributes["orientation"]

        rdata = self.getHeadTail(center)
        rdata0 = self.getHeadTail(center, False)
        dazRna(bone).DazHead = rdata.dazhead
        dazRna(bone).DazAngle = 0

        vec = d2b00(rdata.tail) - d2b00(rdata.head)
        vec0 = d2b00(rdata0.tail) - d2b00(rdata0.head)
        if vec.length > 0 and vec0.length > 0:
            vec /= vec.length
            vec0 /= vec0.length
            sprod = vec.dot(vec0)
            if sprod < -0.99:
                dazRna(bone).DazAngle = pi
                dazRna(bone).DazNormal = vec.cross(vec0)
            elif sprod < 0.99:
                dazRna(bone).DazAngle = math.acos(sprod)
                dazRna(bone).DazNormal = vec.cross(vec0)

        for child in self.children.values():
            if isinstance(child, BoneInstance):
                child.buildBoneProps(rig, center)


    def buildFormulas(self, rig, hide):
        if self.ignoreBone:
            return
        if (self.node.formulas and
            GS.useDefaultDrivers and
            self.name in rig.pose.bones.keys()):
            from .load_morph import buildBoneFormula
            pb = rig.pose.bones[self.name]
            isrot = self.isRotMorph(self.node.formulas)
            pb.rotation_mode = self.getRotationMode(pb, isrot)
            errors = []
            buildBoneFormula(self.node, rig, self.figure.altmorphs, errors)
        if hide or not self.getValue(["Visible"], True):
            self.figure.hiddenBones[self.name] = True
            bone = rig.data.bones[self.name]
            bone.hide = True
        if (self.name.endswith(
                ("twist1", "twist2", "metatarsal", "Metatarsals",
                 "facerig", "FaceRig", "carpal", "hand_anchor")) or
            self.name.startswith(("lCarpal", "rCarpal")) or
            self.name in ["upperteeth", "lowerteeth", "upperTeeth", "lowerTeeth"]):
            bone = rig.data.bones[self.name]
            enableBoneNumLayer(bone, rig, T_TWEAK)
        for child in self.children.values():
            if isinstance(child, BoneInstance):
                child.buildFormulas(rig, hide)


    def isRotMorph(self, formulas):
        for formula in formulas:
            if ("output" in formula.keys() and
                "?rotation" in formula["output"]):
                return True
        return False


    def getRotationMode(self, pb, useEulers):
        if GS.unflipped:
            return self.rotation_order
        elif GS.useQuaternions and pb.name in BD.SocketBones:
            return 'QUATERNION'
        elif useEulers:
            return BD.getDefaultMode(pb)
        else:
            return BD.getDefaultMode(pb)


    def setFlip(self):
        for n in range(3):
            if (self.name.startswith(BD.UnFlips[n]) or
                self.name in BD.UnFlipsSharp[n]):
                self.flipped[n] = False
            elif (self.name.startswith(BD.Flips[n]) or
                self.name in BD.FlipsSharp[n]):
                self.flipped[n] = True


    def buildPose(self, figure, inFace, targets, missing):
        if self.ignoreBone:
            return
        node = self.node
        rig = figure.rna
        if node.name not in rig.pose.bones.keys():
            print("NIX", node.name)
            return
        pb = rig.pose.bones[node.name]
        self.rna = pb
        pb.bone.inherit_scale = self.defaultInherit()
        pb.bone.bbone_x = pb.bone.bbone_z = GS.scale

        mapped = self.node.mapped
        if (mapped and
            self.name != mapped and
            mapped not in dazRna(rig.data).DazBoneMap.keys()):
            pg = dazRna(rig.data).DazBoneMap.add()
            pg.name = mapped
            pg.s = self.name
        if self.id != self.name:
            dazRna(pb.bone).DazTrueName = unquote(self.id)
        if pb.name in figure.driven.keys():
            pb.rotation_mode = self.getRotationMode(pb, True)
            enableBoneNumLayer(pb.bone, rig, T_HIDDEN)
        else:
            pb.rotation_mode = self.getRotationMode(pb, False)
            enableBoneNumLayer(pb.bone, rig, T_BONES)
        dazRna(pb).DazRotMode = self.rotation_order
        dazRna(pb).DazAxes = self.axes
        dazRna(pb).DazFlips = [(-1 if flip else +1) for flip in self.flipped]
        tchildren = self.targetTransform(pb, node, targets, rig)
        self.setRotationLockDaz(pb, rig)
        self.setLocationLockDaz(pb, rig)

        trgname = BD.Irises.get(pb.name)
        if trgname:
            trg = rig.pose.bones.get(trgname)
            if trg:
                from .rig_utils import dampedTrack
                cns = dampedTrack(pb, trg, rig)
                cns.head_tail = 1.0
        for child in self.children.values():
            if isinstance(child, BoneInstance):
                child.buildPose(figure, inFace, tchildren, missing)


    def targetTransform(self, pb, node, targets, rig):
        from .node import setBoneTransform
        tname = getMappedBone(node.name, rig)
        if tname and tname in targets.keys():
            tinst = targets[tname]
            tfm = Transform(
                trans = tinst.attributes["translation"],
                rot = tinst.attributes["rotation"])
            tchildren = tinst.children
        else:
            tinst = None
            tfm = Transform(
                trans = self.attributes["translation"],
                rot = self.attributes["rotation"])
            tchildren = {}
        if LS.fitFile:
            if nonzero(tfm.rot):
                dazRna(pb).DazRestRotation = tfm.rot
        else:
            setBoneTransform(tfm, pb, rig)
            if nonzero(tfm.trans):
                dazRna(pb).DazTranslation = tfm.trans
            if nonzero(tfm.rot):
                dazRna(pb).DazRotation = tfm.rot
        return tchildren


    def formulate(self, key, value):
        from .node import setBoneTransform
        if self.figure is None:
            return
        channel,comp = key.split("/")
        self.attributes[channel][getIndex(comp)] = value
        pb = self.rna
        rig = self.figure.rna
        tfm = Transform(
            trans=self.attributes["translation"],
            rot=self.attributes["rotation"])
        setBoneTransform(tfm, pb, rig)


    def getLocksLimits(self, pb, structs):
        locks = [False, False, False]
        limits = [None, None, None]
        useLimits = False
        for idx,comp in enumerate(structs):
            if "locked" in comp.keys() and comp["locked"]:
                locks[idx] = True
            elif "clamped"in comp.keys() and comp["clamped"]:
                if comp["min"] == 0 and comp["max"] == 0:
                    locks[idx] = True
                else:
                    limits[idx] = (comp["min"], comp["max"])
                    if comp["min"] != -180 or comp["max"] != 180:
                        useLimits = True
        return locks,limits,useLimits


    IndexComp = { 0 : "x", 1 : "y", 2 : "z" }

    def setRotationLockDaz(self, pb, rig):
        locks,limits,useLimits = self.getLocksLimits(pb, self.node.rotation)
        # DazRotLocks used to update lock_rotation
        for n,lock in enumerate(locks):
            idx = self.axes[n]
            dazRna(pb).DazRotLocks[idx] = lock
        if GS.useLockRot:
            for n,lock in enumerate(locks):
                idx = self.axes[n]
                pb.lock_rotation[idx] = lock
        #if pb.rotation_mode == 'QUATERNION':
        #    return
        if useLimits and GS.useLimitRot and not self.isPosed:
            from .rig_utils import limitRotation
            cns = limitRotation(pb, rig)
            setEulerOrder(cns, BD.getDefaultMode(pb))
            for n,limit in enumerate(limits):
                idx = self.axes[n]
                xyz = self.IndexComp[idx]
                if limit is None:
                    setattr(cns, "use_limit_%s" % xyz, False)
                else:
                    mind, maxd = limit
                    if maxd-mind > 359:
                        if GS.verbosity >= 3:
                            print("Unlimited rotation %s %.3f %.3f %.3f" % (pb.name, mind, maxd, maxd-mind))
                        continue
                    minr = mind*D
                    if abs(minr) < 1e-4:
                        minr = 0
                    maxr = maxd*D
                    if abs(maxr) < 1e-4:
                        maxr = 0
                    if self.flipped[n]:
                        tmp = minr
                        minr = -maxr
                        maxr = -tmp
                    setattr(cns, "use_limit_%s" % xyz, True)
                    setattr(cns, "min_%s" % xyz, minr)
                    setattr(cns, "max_%s" % xyz, maxr)
                    if GS.displayLimitRot:
                        setattr(pb, "use_ik_limit_%s" % xyz, True)
                        setattr(pb, "ik_min_%s" % xyz, minr)
                        setattr(pb, "ik_max_%s" % xyz, maxr)


    def setLocationLockDaz(self, pb, rig):
        locks,limits,useLimits = self.getLocksLimits(pb, self.node.translation)
        # DazLocLocks used to update lock_location
        for n,lock in enumerate(locks):
            idx = self.axes[n]
            dazRna(pb).DazLocLocks[idx] = lock
        if GS.useLockLoc:
            for n,lock in enumerate(locks):
                idx = self.axes[n]
                pb.lock_location[idx] = lock
        if useLimits and GS.useLimitLoc:
            from .rig_utils import limitLocation
            cns = limitLocation(pb, rig)
            for n,limit in enumerate(limits):
                idx = self.axes[n]
                xyz = self.IndexComp[idx]
                if limit is None:
                    setattr(cns, "use_min_%s" % xyz, False)
                    setattr(cns, "use_max_%s" % xyz, False)
                else:
                    mind, maxd = limit
                    if self.flipped[n]:
                        tmp = mind
                        mind = -maxd
                        maxd = -tmp
                    setattr(cns, "use_min_%s" % xyz, True)
                    setattr(cns, "use_max_%s" % xyz, True)
                    setattr(cns, "min_%s" % xyz, mind*GS.scale)
                    setattr(cns, "max_%s" % xyz, maxd*GS.scale)

#-------------------------------------------------------------
#   Utilities
#-------------------------------------------------------------

def eulerIsZero(euler):
    vals = [abs(x) for x in euler]
    return (max(vals) < 1e-4)


def setRoll(eb, xaxis):
    yaxis = eb.tail - eb.head
    yaxis.normalize()
    xaxis -= yaxis.dot(xaxis)*yaxis
    xaxis.normalize()
    zaxis = xaxis.cross(yaxis)
    zaxis.normalize()
    mat = Matrix().to_3x3()
    mat.col[0] = xaxis
    mat.col[1] = yaxis
    mat.col[2] = zaxis
    quat = mat.to_quaternion()
    if abs(quat.w) < 1e-4:
        eb.roll = pi
    else:
        eb.roll = 2*math.atan(quat.y/quat.w)

#-------------------------------------------------------------
#   Bone
#-------------------------------------------------------------

class Bone(Node):

    def __init__(self, fileref):
        Node.__init__(self, fileref)
        self.mapped = None
        self.pointAt = None


    def __repr__(self):
        return ("<Bone %s %s>" % (self.id, self.instances))


    def getSelfId(self):
        return self.node.name


    def makeInstance(self, fileref, struct):
        return BoneInstance(fileref, self, struct)


    def getInstance(self, ref, caller=None):
        def getSelfInstance(ref, instances):
            iref = instRef(ref)
            if iref in instances.keys():
                return instances[iref]
            iref = unquote(iref)
            if iref in instances.keys():
                return instances[iref]
            elif iref in BD.BoneMap.keys():
                return instances.get(BD.BoneMap[iref])
            else:
                return None

        iref = getSelfInstance(ref, self.instances)
        if iref:
            return iref
        if self.sourcing:
            iref = getSelfInstance(ref, self.sourcing.instances)
            if iref:
                print("Sourced %s" % iref)
                return iref

        trgfig = self.figure.sourcing
        if trgfig:
            iref = instRef(ref)
            struct = {
                "id" : iref,
                "url" : self.url,
                "target" : trgfig,
            }
            if GS.verbosity >= 3:
                print("Creating reference to target figure:\n", trgfig)
            inst = self.makeInstance(self.fileref, struct)
            self.instances[iref] = inst
            if GS.verbosity >= 3:
                print("Target instance:\n", inst)
            return inst
        if (GS.verbosity <= 2 and
            len(self.instances.values()) > 0):
            return list(self.instances.values())[0]
        msg = ("Bone: Did not find instance %s in %s\nSelf = %s\nSourcing = %s" % (iref, list(self.instances.keys()), self, self.sourcing))
        reportError(msg)
        return None


    def parse(self, struct):
        from .figure import Figure
        Node.parse(self, struct)
        for channel,data in struct.items():
            if channel == "rotation":
                self.rotation = data
            elif channel == "translation":
                self.translation = data
            elif channel == "scale":
                self.scale = data
        if isinstance(self.parent, Figure):
            self.figure = self.parent
        elif isinstance(self.parent, Bone):
            self.figure = self.parent.figure


    def update(self, struct):
        Node.update(self, struct)
        url = struct.get("url")
        if url:
            self.mapped = unquote(url).rsplit("#")[-1]
        pointAt = self.channels.get("Point At")
        if pointAt:
            node = pointAt.get("node")
            if node and node[0] == "#":
                self.figure.pointing[self.name] = node[1:]


    def build(self, context, inst=None):
        pass


    def preprocess(self, context, inst):
        pass


    def poseRig(self, context, inst):
        pass

